package com.example.hotshot_project;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.hotshot_project.dagger.Dagger;
import com.example.hotshot_project.dagger.component.ActivityComponent;
import com.example.hotshot_project.dagger.component.DaggerActivityComponent;
import com.example.hotshot_project.dagger.module.NetworkModule;
import com.example.hotshot_project.dagger.module.ViewModule;


/**
 * Created by Anson on 2016/2/23.
 */
public abstract class BaseActivity extends AppCompatActivity {
    public Dagger dagger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .networkModule(new NetworkModule(this))
                .viewModule(new ViewModule(this))
                .build();

        dagger = activityComponent.dagger();
    }
}
