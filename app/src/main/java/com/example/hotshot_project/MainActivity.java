package com.example.hotshot_project;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hugo.weaving.DebugLog;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@DebugLog
public class MainActivity extends BaseActivity {
    @Bind(R.id.helloTextView) TextView helloTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.testButton)
    public void onClickTestButton() {
        helloTextView.setText("Hello World~");
        testApi();
    }


    private void testApi() {
        dagger.progressDialog.show();
        dagger.apiService.getInstance()
                .test()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(() -> dagger.progressDialog.cancel())
                .subscribe(new Observer<Objects>() {
                    @Override
                    public void onCompleted() {
                        //TODO do something or not
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof RetrofitError) {
                            Response response = ((RetrofitError) e).getResponse();
                            if (response != null) {
                                switch (response.getStatus()) {
                                    case 401:
                                        //TODO Handle 401 error
                                        break;
                                    case 404:
                                        //TODO Handle 404 error
                                        break;
                                    default:
                                        //TODO Handle other status error
                                        break;
                                }
                                Toast.makeText(MainActivity.this, "Status code : " + response.getStatus(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Cannot connection.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onNext(Objects objects) {
                        //TODO do something
                    }
                });
    }
}
