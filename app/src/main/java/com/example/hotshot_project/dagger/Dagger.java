package com.example.hotshot_project.dagger;

import android.app.ProgressDialog;

import com.example.hotshot_project.service.ApiService;

import javax.inject.Inject;

/**
 * Created by Anson on 2016/2/23.
 */
public class Dagger {

    @Inject
    public Dagger() {
    }

    public @Inject ApiService apiService;

    public @Inject ProgressDialog progressDialog;
}
