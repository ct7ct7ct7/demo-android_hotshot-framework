package com.example.hotshot_project.dagger.module;

import android.content.Context;

import com.example.hotshot_project.service.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class NetworkModule {

    private Context context;

    public NetworkModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return new ApiService(context);
    }
}
