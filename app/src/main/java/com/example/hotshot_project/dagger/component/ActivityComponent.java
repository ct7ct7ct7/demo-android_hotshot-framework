package com.example.hotshot_project.dagger.component;


import com.example.hotshot_project.dagger.Dagger;
import com.example.hotshot_project.dagger.module.NetworkModule;
import com.example.hotshot_project.dagger.module.ViewModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Anson on 2016/1/28.
 */
@Singleton
@Component(modules = {NetworkModule.class, ViewModule.class} )
public interface ActivityComponent {
    Dagger dagger();
}
