package com.example.hotshot_project.dagger.module;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.hotshot_project.R;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Anson on 2016/2/23.
 */
@Module
public class ViewModule {

    private Context context;

    public ViewModule(Context context) {
        this.context = context;
    }

    @Provides
    ProgressDialog provideLoadingProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getString(R.string.loading_dialog_title));
        progressDialog.setIcon(R.drawable.icon);
        progressDialog.setMessage(context.getString(R.string.loading_dialog_message));
        return progressDialog;
    }
}
