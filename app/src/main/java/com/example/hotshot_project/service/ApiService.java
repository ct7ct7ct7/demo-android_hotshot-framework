package com.example.hotshot_project.service;

import android.content.Context;

import com.example.hotshot_project.BuildConfig;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


/**
 * Created by Anson Chen on 2016/1/21.
 */
public class ApiService {
    private Instance instance;

    public ApiService(Context context) {
        final int CACHE_SIZE = 10 * 1024 * 1024;
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(15, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(15, TimeUnit.SECONDS);
        okHttpClient.setCache(new Cache(context.getExternalCacheDir(), CACHE_SIZE));

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("URL Endpoint")//TODO Set your endpoint.
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setClient(new OkClient(okHttpClient))
                .build();
        instance = restAdapter.create(Instance.class);
    }

    public Instance getInstance() {
        return instance;
    }
}
