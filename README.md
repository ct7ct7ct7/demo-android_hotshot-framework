# README #

這個 example 使用了幾個目前主流的 Library

來幫助你快速開發 Android


### clone後的設定步驟 ###
1. 因為有使用Dagger，所以clone後請在Android Studio 內 Rebuild Project 一次
2. 到 service/ApiService 設定URL端點
3. 到 service/Instance 設定URL路徑與調整 http method


### Core Library ###
* RxJava
* Dagger2
* Retrofit / okhttp


### Other Library ###

請自己視情況增減與使用

* RetroLambda
* Hugo
* Glide
* EventBus
* ButterKnife
* Gson
* EventBus
* ButterKnife